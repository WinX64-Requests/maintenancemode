/*
 *   MaintenanceMode - Enable maintenance mode on your server
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.mm.command;

import java.util.concurrent.TimeUnit;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import io.github.winx64.mm.MaintenanceMode;
import io.github.winx64.mm.configuration.MaintenanceMessages;
import io.github.winx64.mm.configuration.MaintenanceMessages.Message;
import io.github.winx64.mm.util.Util;

/**
 * Command /maintenance's executor
 * 
 * @author WinX64
 *
 */
public final class CommandMaintenance implements CommandExecutor {

	private final MaintenanceMode plugin;
	private final MaintenanceMessages messages;

	public CommandMaintenance(MaintenanceMode plugin) {
		this.plugin = plugin;
		this.messages = plugin.getMessages();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length < 1) {
			if (plugin.isMaintenanceActive()) {
				int start = (int) (System.currentTimeMillis() - plugin.getMaintenanceStart()) / 1000;
				int period = (int) plugin.getMaintenancePeriod();
				String startString = Util.formatCompleteTimeString(start, messages);
				String periodString = Util.formatCompleteTimeString(period, messages);
				sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_CURRENT_INFO, startString, periodString));
			} else {
				sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_NOT_ENABLED));
				sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_TO_START, alias));
			}
			return true;
		}

		String option = args[0].toLowerCase();
		String stopArgument = messages.get(Message.COMMAND_MAINTENANCE_STOP_ARGUMENT);
		if (option.equalsIgnoreCase(stopArgument)) {
			if (!plugin.isMaintenanceActive()) {
				sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_NOT_ENABLED));
				sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_TO_START, alias));
				return true;
			}

			sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_STOPPED));
			plugin.stopMaintenance();
			return true;
		}

		if (plugin.isMaintenanceActive()) {
			sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_ALREADY_ENABLED));
			sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_TO_STOP, alias, stopArgument));
			return true;
		}

		int period = Util.parseDurationString(option, TimeUnit.SECONDS);
		if (period == -1) {
			sender.sendMessage(messages.get(Message.TIME_INVALID_DURATION, option));
			return true;
		}

		sender.sendMessage(messages.get(Message.COMMAND_MAINTENANCE_STARTED));
		plugin.startMaintenance(period);
		return true;
	}
}
