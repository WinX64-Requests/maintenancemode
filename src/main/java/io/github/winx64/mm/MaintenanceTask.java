/*
 *   MaintenanceMode - Enable maintenance mode on your server
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.mm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import io.github.winx64.mm.configuration.MaintenanceMessages;
import io.github.winx64.mm.configuration.MaintenanceMessages.Message;
import io.github.winx64.mm.util.Util;

/**
 * Helper class to handle the countdown and post maintenance tasks
 * 
 * @author WinX64
 *
 */
public final class MaintenanceTask extends BukkitRunnable {

	private final MaintenanceMessages messages;
	private int seconds;

	public MaintenanceTask(MaintenanceMode plugin, int seconds) {
		this.messages = plugin.getMessages();
		this.seconds = seconds;
	}

	@Override
	public void run() {
		if (seconds < 1) {
			startMaintenanceLock();
		} else if (seconds % 60 == 0) {
			broadcastTimeLeft(true);
		} else if (seconds <= 30 && seconds % 10 == 0) {
			broadcastTimeLeft(true);
		} else if (seconds <= 5) {
			broadcastTimeLeft(false);
		}
		this.seconds--;
	}

	private void broadcastTimeLeft(boolean complete) {
		String timeLeft = complete ? Util.formatTimeString(seconds) : Integer.toString(seconds);
		Bukkit.broadcastMessage(messages.get(Message.GENERAL_MAINTENANCE_ACTIVATES_IN, timeLeft));
	}

	private void startMaintenanceLock() {
		List<Player> pendingPlayers = new ArrayList<Player>();
		Set<OfflinePlayer> whitelistedPlayers = Bukkit.getWhitelistedPlayers();
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!player.isOp() && !whitelistedPlayers.contains(player)) {
				pendingPlayers.add(player);
			}
		}

		Bukkit.setWhitelist(true);

		Iterator<Player> iter = pendingPlayers.iterator();
		String kickMessage = messages.get(Message.GENERAL_MAINTENANCE_KICK);
		while (iter.hasNext()) {
			Player player = iter.next();
			player.kickPlayer(kickMessage);
			iter.remove();
		}

		Bukkit.broadcastMessage(messages.get(Message.GENERAL_MAINTENANCE_ACTIVE));
		this.cancel();
	}
}
