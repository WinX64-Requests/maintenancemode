/*
 *   MaintenanceMode - Enable maintenance mode on your server
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.mm;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import io.github.winx64.mm.command.CommandMaintenance;
import io.github.winx64.mm.command.CommandReload;
import io.github.winx64.mm.configuration.MaintenanceConfiguration;
import io.github.winx64.mm.configuration.MaintenanceMessages;
import io.github.winx64.mm.configuration.MaintenanceMessages.Message;
import io.github.winx64.mm.util.Util;

/**
 * MaintenanceMode's main class
 * 
 * @author WinX64
 *
 */
public final class MaintenanceMode extends JavaPlugin {

	private final Logger logger;
	private final MaintenanceConfiguration configuration;
	private final MaintenanceMessages messages;

	private long maintenanceStart;
	private int maintenancePeriod;
	private BukkitTask maintenanceTask;

	private boolean startupFailed;

	public MaintenanceMode() {
		this.logger = getLogger();
		this.configuration = new MaintenanceConfiguration(this);
		this.messages = new MaintenanceMessages(this);

		this.maintenanceStart = 0;
		this.maintenancePeriod = 0;
		this.maintenanceTask = null;

		this.startupFailed = false;
	}

	@Override
	public void onEnable() {
		if (!configuration.loadConfiguration()) {
			halt("Configuration load error");
			return;
		}

		if (!messages.loadMessages()) {
			halt("Messages load error");
			return;
		}

		getCommand("maintenance").setExecutor(new CommandMaintenance(this));
		getCommand("maintenance-reload").setExecutor(new CommandReload(this));

		if (configuration.shouldTurnOffOnEnable()) {
			Bukkit.setWhitelist(false);
		}
	}

	@Override
	public void onDisable() {
		if (!startupFailed && configuration.shouldTurnOffOnDisable()) {
			Bukkit.setWhitelist(false);
		}
	}

	private void halt(String reason) {
		log(Level.SEVERE, "The plugin is going to be disabled! Reason: %s", reason);
		this.startupFailed = true;
		Bukkit.getPluginManager().disablePlugin(this);
	}

	public MaintenanceConfiguration getConfiguration() {
		return configuration;
	}

	public MaintenanceMessages getMessages() {
		return messages;
	}

	public void startMaintenance(int period) {
		if (maintenanceTask != null) {
			return;
		}

		String periodString = Util.formatCompleteTimeString(period, messages);
		Bukkit.broadcastMessage(messages.get(Message.GENERAL_MAINTENANCE_STARTED, periodString));

		this.maintenanceStart = System.currentTimeMillis();
		this.maintenancePeriod = period;

		this.maintenanceTask = new MaintenanceTask(this, period).runTaskTimer(this, 0, 20);
	}

	public void stopMaintenance() {
		if (maintenanceTask == null) {
			return;
		}

		Bukkit.broadcastMessage(messages.get(Message.GENERAL_MAINTENANCE_STOPPED));
		Bukkit.setWhitelist(false);

		this.maintenanceTask.cancel();
		this.maintenanceTask = null;
	}

	public boolean isMaintenanceActive() {
		return maintenanceTask != null;
	}

	public long getMaintenanceStart() {
		return maintenanceStart;
	}

	public int getMaintenancePeriod() {
		return maintenancePeriod;
	}

	public void log(String format, Object... args) {
		log(Level.INFO, null, String.format(format, args));
	}

	public void log(String message) {
		log(Level.INFO, null, message);
	}

	public void log(Level level, String format, Object... args) {
		log(level, null, String.format(format, args));
	}

	public void log(Level level, Exception e, String format, Object... args) {
		log(level, e, String.format(format, args));
	}

	public void log(Level level, String message) {
		log(level, null, message);
	}

	public void log(Level level, Exception e, String message) {
		logger.log(level, message, e);
	}
}
