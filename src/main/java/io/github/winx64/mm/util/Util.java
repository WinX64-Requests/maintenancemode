/*
 *   MaintenanceMode - Enable maintenance mode on your server
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.mm.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import io.github.winx64.mm.configuration.MaintenanceMessages;
import io.github.winx64.mm.configuration.MaintenanceMessages.Message;

public final class Util {

	private static final Pattern DURATION_PATTERN = Pattern.compile("(?<=\\D)(?=\\d)*|(?<=\\d)*(?=\\D)");

	private static final Map<Character, TimeUnit> UNITS_BY_CHARACTER;

	static {
		Map<Character, TimeUnit> unitsByChar = new HashMap<Character, TimeUnit>();
		unitsByChar.put('s', TimeUnit.SECONDS);
		unitsByChar.put('m', TimeUnit.MINUTES);
		unitsByChar.put('h', TimeUnit.HOURS);

		UNITS_BY_CHARACTER = Collections.unmodifiableMap(unitsByChar);
	}

	public static void main(String[] args) {
		System.out.println(Util.formatTimeString(558));
	}

	private Util() {}

	public static int parseDurationString(String durationString, TimeUnit outputUnit) {
		String[] args = DURATION_PATTERN.split(durationString);
		if (args.length % 2 != 0) {
			return -1;
		}
		int totalUnits = 0;
		for (int i = 0; i < args.length - 1; i += 2) {
			Long sectionTime = Long.parseLong(args[i]);
			Character sectionChar = Character.toLowerCase(args[i + 1].charAt(0));
			TimeUnit sectionUnit = UNITS_BY_CHARACTER.get(sectionChar);
			if (sectionTime == null || sectionChar == null || sectionUnit == null) {
				return -1;
			}
			totalUnits += outputUnit.convert(sectionTime, sectionUnit);
		}
		return totalUnits;
	}

	public static String formatTimeString(int totalSeconds) {
		int minutes = totalSeconds / 60;
		int seconds = totalSeconds % 60;

		return String.format("%02d:%02d", minutes, seconds);
	}

	public static String formatCompleteTimeString(int totalSeconds, MaintenanceMessages messages) {
		StringBuilder builder = new StringBuilder();
		for (CompleteTimeUnit compUnit : CompleteTimeUnit.values()) {
			long unitTime = compUnit.unit.convert(totalSeconds, TimeUnit.SECONDS);
			if (unitTime > 0) {
				builder.append(unitTime).append(' ')
						.append(messages.get(unitTime == 1 ? compUnit.singular : compUnit.plural)).append(' ');
			}
			totalSeconds -= TimeUnit.SECONDS.convert(unitTime, compUnit.unit);
		}
		return builder.toString().trim();
	}

	private static enum CompleteTimeUnit {
		HOURS(TimeUnit.HOURS, Message.TIME_HOURS, Message.TIME_HOUR),
		MINUTES(TimeUnit.MINUTES, Message.TIME_MINUTES, Message.TIME_MINUTE),
		SECONDS(TimeUnit.SECONDS, Message.TIME_SECONDS, Message.TIME_SECOND);

		private final TimeUnit unit;
		private final Message plural;
		private final Message singular;

		private CompleteTimeUnit(TimeUnit unit, Message plural, Message singular) {
			this.unit = unit;
			this.plural = plural;
			this.singular = singular;
		}
	}
}
