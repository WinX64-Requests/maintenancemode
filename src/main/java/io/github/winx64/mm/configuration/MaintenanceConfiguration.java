/*
 *   MaintenanceMode - Enable maintenance mode on your server
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.mm.configuration;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import io.github.winx64.mm.MaintenanceMode;

/**
 * Data class that holds all the configuration-related variables of
 * MaintenanceMode
 * 
 * @author WinX64
 *
 */
public final class MaintenanceConfiguration {

	private static final String CONFIG_FILE_NAME = "config.yml";
	private static final String OLD_CONFIG_FILE_NAME = "config-old-%d.yml";
	private static final String CONFIG_VERSION_KEY = "config-version";
	private static final int CONFIG_VERSION = 1;

	private final MaintenanceMode plugin;
	private final File configFile;
	private FileConfiguration config;

	private boolean turnOffOnEnable;
	private boolean turnOffOnDisable;

	public MaintenanceConfiguration(MaintenanceMode plugin) {
		this.plugin = plugin;
		this.configFile = new File(plugin.getDataFolder(), CONFIG_FILE_NAME);

		this.turnOffOnEnable = false;
		this.turnOffOnDisable = false;
	}

	/**
	 * Returns if the whitelist should be disabled whenever the plugin starts up
	 * 
	 * @return Whether it should be disabled or not
	 */
	public boolean shouldTurnOffOnEnable() {
		return turnOffOnEnable;
	}

	/**
	 * Returns if the whitelist should be disabled whenever the plugin stops
	 * 
	 * @return Whether it should be disabled or not
	 */
	public boolean shouldTurnOffOnDisable() {
		return turnOffOnDisable;
	}

	/**
	 * Attempts to load the configuration
	 * 
	 * @return Whether it was successful or not
	 */
	public boolean loadConfiguration() {
		try {
			if (!configFile.exists()) {
				plugin.log(Level.INFO, "[Config] Config file not found. Creating a new one...");
				plugin.saveResource(CONFIG_FILE_NAME, true);
			}

			this.config = YamlConfiguration.loadConfiguration(configFile);
			if (config.getKeys(false).size() == 0) {
				plugin.log(Level.SEVERE, "[Config] Empty configuration! Did any error happen while parsing it?");
				return false;
			}

			if (!ensureCorrectVersion(true)) {
				plugin.log(Level.SEVERE, "[Config] Could not load the correct version of the configuration!",
						CONFIG_VERSION);
				return false;
			}

			loadWhitelist(config.getConfigurationSection("whitelist-configuration"));

			plugin.log(Level.INFO, "[Config] Configuration loaded successfully!");
			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e, "An error occurred while trying to load the configuration! Details below:");
			return false;
		}
	}

	/**
	 * Ensures that MaintenanceMode is reading the correct version of the
	 * configuration file
	 * 
	 * @param saveAndRetry
	 * @return Whether it was successful or not
	 */
	private boolean ensureCorrectVersion(boolean saveAndRetry) {
		int currentVersion = config.getInt(CONFIG_VERSION_KEY, -1);
		if (currentVersion == -1 && saveAndRetry) {
			plugin.log(Level.WARNING, "[Config] The configuration version is missing. Did you erase it by accident?");
			plugin.log(Level.INFO, "[Config] Creating an up to date one...");
			plugin.saveResource(CONFIG_FILE_NAME, true);
			this.config = YamlConfiguration.loadConfiguration(configFile);
			return ensureCorrectVersion(false);
		}

		if (currentVersion != CONFIG_VERSION) {
			if (saveAndRetry) {
				plugin.log(Level.WARNING, "[Config] Outdated configuration detected. Preparing to create a new one...");
				if (!moveOldConfiguration()) {
					plugin.log(Level.WARNING, "[Config] Failed to move old configuration. Overwritting it...");
				}
				plugin.saveResource(CONFIG_FILE_NAME, true);
				this.config = YamlConfiguration.loadConfiguration(configFile);
				return ensureCorrectVersion(false);
			} else {
				return false;
			}
		}

		return true;
	}

	/**
	 * Renames the old configuration with the current timestamp
	 * 
	 * @return Whether it was successful or not
	 */
	private boolean moveOldConfiguration() {
		try {
			String newFileName = String.format(OLD_CONFIG_FILE_NAME, System.currentTimeMillis());
			File newFile = new File(plugin.getDataFolder(), newFileName);
			plugin.log(Level.INFO, "[Config] The old %s is now \"%s\"", CONFIG_FILE_NAME, newFileName);
			configFile.renameTo(newFile);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Loads the 'whitelist-configuration' section
	 * 
	 * @param whitelistSection
	 *            The whitelist section
	 */
	private void loadWhitelist(ConfigurationSection whitelistSection) {
		if (whitelistSection == null) {
			plugin.log(Level.WARNING, "The 'whitelist-configuration' section is missing! Assuming default values.");
			return;
		}

		if (whitelistSection.contains("turn-off-on-enable")) {
			this.turnOffOnEnable = whitelistSection.getBoolean("turn-off-on-enable");
		} else {
			plugin.log(Level.WARNING, "The 'turn-off-on-enable' option is missing! Using default value of %d.",
					turnOffOnEnable);
		}

		if (whitelistSection.contains("turn-off-on-disable")) {
			this.turnOffOnDisable = whitelistSection.getBoolean("turn-off-on-disable");
		} else {
			plugin.log(Level.WARNING, "The 'turn-off-on-disable' option is missing! Using default value of %d.",
					turnOffOnDisable);
		}
	}
}
